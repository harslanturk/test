    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" onclick="sendlogin();">Send</button>
        </div>
        </div>
    </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" 
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" 
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        function sendlogin() {
        var email = $('#exampleInputEmail1').val();
        var password = $('#exampleInputPassword1').val();        
        if(email=="")
        {
            alert("Please fill 'E-Mail' input");
        }
        else if(password=="")
        {
            alert("Please fill 'Phone' input");
        }
        else
        {
            $.post("/check-login", {
                email: email,
                password: password
            }, function (result) {
                console.log(result);
                if(result == "success")
                {
                    window.location.href = '/articles';
                    alert("Successful, you are being forwarded.");
                    $('#exampleInputEmail1').val("");
                    $('#exampleInputPassword1').val("");
                }
                else{
                    alert(result);
                }
            });
        }
    }

    function sendLogout() {
        $.post("/logout", function (result) {            
            window.location.href = '/articles';
        });
        
    }
    </script>
</body>

</html>