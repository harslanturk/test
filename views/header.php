<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.png">
    <title>Check24</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/edit.css">
</head>

<body>

    <section class="header-area">
        
        <div class="row">
            <div class="col-1">
                <img src="https://via.placeholder.com/150" style="width: 100%;"/>
            </div>
            <div class="col-11">
                <h2>Check24 Blog</h2>
            </div>
        </div>

    </section>

    <section class="menu-area">
        
        <div class="row">
            <div class="col-4">
                <div class="row">                
                    <div class="col-4"><a href="/articles">Overview</a></div>
                    <div class="col-4"><a href="#">New Article</a></div>
                    <div class="col-4"><a href="#">Imprint</a></div>
                </div>
            </div>
            <div class="col-8">
                <div class="login-area">
                <?php    
                    if(isset($_SESSION["user_id"]))
                    {
                        echo 'Welcome '.$_SESSION["user_name"].' <a href="#" onclick="sendLogout();"> Logout</a>';
                    }
                    else{
                        echo '<a href="#" data-toggle="modal" data-target="#exampleModal">Login</a>';
                    }
                ?>
                </div>
            </div>
        </div>

    </section>