<?php
require_once APP_ROOT . '/views/header.php';
?>

    <section class="content-area">
        <?php
        foreach($currentPageResults as $key => $value)
        {
        $content = substr($value['content'], 0, 1000);     
        echo '
        <div class="row article">
            <div class="col-10">
                <div class="content-title">'.$value['created_at']." | ".$value['title'].'</div>
                <div class="content-text">'.$content.'</div>
                <div class="content-footer">
                    <div class="row">
                        <div class="col-6">
                            Author: '.$value['name'].' '.$value['surname'].'
                        </div>
                        <div class="col-6"  style="text-align:right">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <a href="/show-article/'.$value['id'].'"><img src="
                '.$value['url'].'" style="width:100%;"></a>                
            </div>
        </div>
        ';
        }
        ?>
        <div class="row">
            <?php
            if($statusPagination)
            {
                for($i=1; $i<=$numbersPagination; $i++)
                {
                    echo '<a href="/articles/'.$i.'" style="font-weight:bold;">'.$i.'</a> | ';
                }
            }
            ?>
        </div>
    </section>
<?php
require_once APP_ROOT . '/views/footer.php';
?>