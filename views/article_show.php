<?php
require_once APP_ROOT . '/views/header.php';
?>

    <section class="content-area">
        <?php
        $content = $getArticle['content'];     
        echo '
        <div class="row article">
            <div class="col-10">
                <div class="content-title">'.$getArticle['created_at']." | ".$getArticle['title'].'</div>
                <div class="content-text">'.$content.'</div>
                <div class="content-footer">
                    <div class="row">
                        <div class="col-6">
                            Author: '.$getArticle['name'].' '.$getArticle['surname'].'
                        </div>
                        <div class="col-6"  style="text-align:right">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <img src="
                '.$getArticle['url'].'" style="width:100%;">                
            </div>
        </div>
        ';        
        ?>
        
    </section>
<?php
require_once APP_ROOT . '/views/footer.php';
?>