-- --------------------------------------------------------
-- Sunucu:                       127.0.0.1
-- Sunucu sürümü:                10.4.14-MariaDB - mariadb.org binary distribution
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- test1 için veritabanı yapısı dökülüyor
CREATE DATABASE IF NOT EXISTS `test1` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `test1`;

-- tablo yapısı dökülüyor test1.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_articles_users` (`user_id`),
  CONSTRAINT `FK_articles_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- test1.articles: ~4 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `user_id`, `url`, `title`, `content`, `created_at`, `updated_at`) VALUES
	(1, 1, 'https://via.placeholder.com/150', 'Test Article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vestibulum blandit leo eget tincidunt. Mauris scelerisque posuere leo, vel maximus augue. Praesent id ipsum metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac dictum ante. Nullam semper congue convallis. Nam auctor sed dolor quis suscipit. Morbi eu orci nec libero tristique blandit. Mauris quis placerat nunc. Donec vel nisi ullamcorper velit laoreet cursus vitae in sapien.\r\n\r\nProin a dui in erat bibendum blandit. Nulla enim tortor, sodales eget sodales sed, accumsan nec massa. Sed sit amet lorem velit. Sed dignissim cursus nisi, vitae mollis mauris ultrices efficitur. Sed porta ornare egestas. In hac habitasse platea dictumst. Aenean sem metus, feugiat vitae ultricies vel, laoreet eu nisl.\r\n\r\nDonec vel felis non ex placerat sagittis. Sed ipsum metus, scelerisque eu urna sit amet, placerat elementum orci. In consectetur tincidunt imperdiet. Pellentesque tincidunt vehicula nunc, sit amet varius elit porttitor ut. Vestibulum eget purus in urna rutrum vestibulum eget quis est. Donec lectus mauris, efficitur sed congue in, sodales sit amet tortor. Aliquam suscipit lorem nec tellus varius, eget facilisis ex dictum. Aliquam faucibus molestie dui. Etiam nulla tortor, pellentesque sit amet ipsum et, sagittis scelerisque lectus. Maecenas fringilla lobortis massa, ac efficitur urna. In elit nibh, fringilla et ullamcorper at, lacinia ac mi. Donec pretium leo vulputate dui accumsan, ut volutpat mauris condimentum. Donec iaculis nisl vel auctor cursus.\r\n\r\nPraesent scelerisque libero a neque vulputate convallis. Nam auctor nibh enim, quis luctus nibh feugiat id. Phasellus in ex tincidunt, egestas arcu id, feugiat nisl. Integer faucibus pulvinar enim, ac congue ipsum faucibus in. Curabitur et convallis nisl, et luctus nisl. Morbi tincidunt purus vel arcu gravida, at elementum felis pretium. Phasellus tincidunt eros ante, rutrum sagittis mauris suscipit id. Aliquam erat volutpat. In sed arcu eu ipsum gravida facilisis sed sit amet ipsum. In viverra odio ut tortor tempus, nec tempus libero posuere. Suspendisse eget ornare libero. Nullam accumsan odio sapien, vitae pellentesque quam porta consectetur.\r\n\r\nAliquam sodales, nisl ut pulvinar congue, nisi mauris scelerisque purus, ut maximus ipsum nibh non elit. Integer ac mauris nec nulla bibendum maximus. Fusce et nulla ante. Pellentesque odio felis, pretium a turpis aliquet, porta lobortis lorem. Sed tincidunt enim sed augue laoreet pretium. Aliquam auctor quis quam id vulputate. Duis in consectetur nunc. Quisque congue sed orci at suscipit. Mauris dapibus vitae nulla quis tempus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec mauris tellus, consectetur vel augue vitae, condimentum porttitor ante. In hac habitasse platea dictumst.', '2021-07-22 11:34:09', '2021-07-22 11:34:11'),
	(2, 1, 'https://via.placeholder.com/150', 'What is COVID?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vestibulum blandit leo eget tincidunt. Mauris scelerisque posuere leo, vel maximus augue. Praesent id ipsum metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac dictum ante. Nullam semper congue convallis. Nam auctor sed dolor quis suscipit. Morbi eu orci nec libero tristique blandit. Mauris quis placerat nunc. Donec vel nisi ullamcorper velit laoreet cursus vitae in sapien.', '2021-07-12 11:34:09', '2021-07-12 11:34:09'),
	(3, 1, 'https://via.placeholder.com/150', 'Why should we be vaccinated?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vestibulum blandit leo eget tincidunt. Mauris scelerisque posuere leo, vel maximus augue. Praesent id ipsum metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac dictum ante. Nullam semper congue convallis. Nam auctor sed dolor quis suscipit. Morbi eu orci nec libero tristique blandit. Mauris quis placerat nunc. Donec vel nisi ullamcorper velit laoreet cursus vitae in sapien.', '2021-07-11 11:34:09', '2021-07-11 11:34:09'),
	(4, 1, 'https://via.placeholder.com/150', 'Where shoul we go to this summer?', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vestibulum blandit leo eget tincidunt. Mauris scelerisque posuere leo, vel maximus augue. Praesent id ipsum metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ac dictum ante. Nullam semper congue convallis. Nam auctor sed dolor quis suscipit. Morbi eu orci nec libero tristique blandit. Mauris quis placerat nunc. Donec vel nisi ullamcorper velit laoreet cursus vitae in sapien.', '2021-07-10 11:34:09', '2021-07-10 11:34:09');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- tablo yapısı dökülüyor test1.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`),
  KEY `article` (`article_id`),
  CONSTRAINT `article` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- test1.comments: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- tablo yapısı dökülüyor test1.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- test1.users: ~1 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password`, `created_at`, `updated_at`) VALUES
	(1, 'Hasan', 'Arslantürk', 'harslantrk@yahoo.com', '123456', '2021-07-22 11:32:38', '2021-07-22 11:32:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
