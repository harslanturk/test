<?php
//site name
define('SITE_NAME', 'CHECK24');

//App Root
define('APP_ROOT', dirname(dirname(__FILE__)));
define('URL_ROOT', '/');
define('URL_SUBFOLDER', '');

//DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'test1');
// FOR PDO
define('DB_DSN', "mysql:host=localhost;dbname=test1");
define('DB_OPTION', array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
  ));
  session_start();

?>