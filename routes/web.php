<?php 

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

// Routes system
$routes = new RouteCollection();
$routes->add('product', new Route(constant('URL_SUBFOLDER') . '/product/{id}', array('controller' => 'ProductController', 'method'=>'showAction')));
$routes->add('articles', new Route(constant('URL_SUBFOLDER') . '/articles/{page}', array('controller' => 'ArticleController', 'method'=>'index', 'page'=> 1)));
$routes->add('show-article', new Route(constant('URL_SUBFOLDER') . '/show-article/{id}', array('controller' => 'ArticleController', 'method'=>'showArticle')));

$routes->add('logout', new Route(constant('URL_SUBFOLDER') . '/logout', array('controller' => 'LoginController', 'method'=>'logout')));
$routes->add('check-login', new Route(constant('URL_SUBFOLDER') . '/check-login', array('controller' => 'LoginController', 'method'=>'checkLogin')));