<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\User;
use App\Models\Article;
use App\Models\Comment;
use Symfony\Component\Routing\RouteCollection;
use PDO;
use Session;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

class ArticleController extends Controller
{
    public function index($page)
    {
        // List all articles
        $sql = "SELECT articles.id, articles.user_id, users.name, users.surname, users.email, articles.url, articles.title, articles.content, articles.created_at, articles.updated_at FROM articles 
        INNER JOIN users on articles.user_id = users.id
        ORDER BY articles.created_at ASC";
        
        $article = new Article();
        $articles = $article->list($sql);

        // Articles pagination
        $adapter = new ArrayAdapter($articles);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(3); // 3 articles per page
        $pagerfanta->setCurrentPage($page); // set current page number
        $currentPageResults = $pagerfanta->getCurrentPageResults();
        $statusPagination = $pagerfanta->haveToPaginate();
        $numbersPagination = $pagerfanta->getNbPages();

        require_once APP_ROOT . '/views/article.php';
    }

    // Show the product attributes based on the id.
	public function showArticle(int $id, RouteCollection $routes)
	{
        // Article detail show

        $filter = ['id' => $id];
        $sql = "SELECT articles.id, articles.user_id, users.name, users.surname, users.email, articles.url, articles.title, articles.content, articles.created_at, articles.updated_at FROM articles 
        INNER JOIN users on articles.user_id = users.id
        WHERE articles.id = :id
        ORDER BY articles.created_at ASC";
        $article = new Article();
        $getArticle = $article->read($sql, $filter);

        require_once APP_ROOT . '/views/article_show.php';
	}

}