<?php 

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\User;
use App\Models\Article;
use App\Models\Comment;
use Symfony\Component\Routing\RouteCollection;
use PDO;
use Session;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
class LoginController extends Controller
{
    public function checkLogin()
    {   
        // Check login data
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $sql = "SELECT *
        FROM users
        WHERE email = :email";

        $filter = ['email' => $email];
        
        $user = new User();
        $getUser = $user->read($sql, $filter);

        if(isset($getUser) && $getUser != NULL)
        {
            $_SESSION["user_id"] = $getUser['id'];
            $_SESSION["user_name"] = $getUser['name']." ".$getUser['surname'];
            $_SESSION["user_email"] = $getUser['email'];

            echo "success";
        }
        else
        {
            echo 'Your "Password" or "E-Mail" is wrong, please try again';
        }
    }

    public function logout(RouteCollection $routes)
    {
        // Logout 
        session_destroy();
    }

}