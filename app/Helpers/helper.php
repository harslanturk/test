<?php
namespace App\Helpers;

class Helper
{

    public static function DateConvertDMY($dt)
    {
        return date_format (date_create ($dt), 'd-m-Y');
    }
    public static function DateConvertYMD($dt)
    {
        return date_format (date_create ($dt), 'Y-m-d');
    }
    // Zaman farkını türkçe olarak geri döndürme.
    public static function trDiff($tarih)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->diffForHumans();
        return $result;
    }

    public static function carbonFormat($tarih,$format)
    {
        Carbon::setLocale('tr');
        $result = Carbon::parse($tarih)->format($format);
        return $result;
    }


    public static function first_day_of_month()
    {
        $start = new Carbon('first day of this month');
        Carbon::setLocale('tr');
        $result = Carbon::parse($start)->format("Y-m-d");

        return $result;

    }

    public static function last_day_of_month()
    {
        $end = new Carbon('last day of this month');
        Carbon::setLocale('tr');
        $result = Carbon::parse($end)->format("Y-m-d");

        return $result;

    }

    public static function later_today_of_month($month)
    {
        $result = Carbon::now()->addMonths($month);
        Carbon::setLocale('tr');
        $result = Carbon::parse($result)->format("Y-m-d");

        return $result;

    }

    public static function day_of_month($month, $yil)
    {
        $start = new Carbon('first day of '.$month.' '.$yil);
        $end = new Carbon('last day of '.$month.' '.$yil);
        Carbon::setLocale('tr');
        $result['first'] = Carbon::parse($start)->format("Y-m-d");
        $result['end'] = Carbon::parse($end)->format("Y-m-d");
        return $result;
    }

    public static function first_end_day_of_month($yil)
    {
        $start = new Carbon('first day of January'.' '.$yil);
        $end = new Carbon('last day of December'.' '.$yil);
        Carbon::setLocale('tr');
        $result['first'] = Carbon::parse($start)->format("Y-m-d");
        $result['end'] = Carbon::parse($end)->format("Y-m-d");
        return $result;
    }


}