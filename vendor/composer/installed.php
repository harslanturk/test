<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'check24/test1',
  ),
  'versions' => 
  array (
    'check24/test1' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'pagerfanta/core' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/doctrine-collections-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/doctrine-dbal-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/doctrine-mongodb-odm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/doctrine-orm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/doctrine-phpcr-odm-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/elastica-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/pagerfanta' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '37e7b90c319234913489a7f94334c632f3b6bf3b',
    ),
    'pagerfanta/solarium-adapter' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'pagerfanta/twig' => 
    array (
      'replaced' => 
      array (
        0 => 'v3.2.0',
      ),
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eca0bf41ed421bed1b57c4958bab16aa86b757d0',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '368e81376a8e049c37cb80ae87dbfbf411279199',
    ),
  ),
);
