<?php
namespace Database;

use App\Interfaces\ModelInterface;
use PDO;

abstract class Model implements ModelInterface
{ 
     // CRUD OPERATIONS

     public function list($sql)
     {
          // MySQL list operation
        try {
            $connection = new PDO(DB_DSN, DB_USER, DB_PASS, DB_OPTION);    
    
            $statement = $connection->prepare($sql);
            $statement->execute();
    
            $result = $statement->fetchAll();

            return $result;
    
            } catch(PDOException $error) {
            return $sql . "<br>" . $error->getMessage();
            }
     }

     public function read($sql, $filter = array())
     {
          // MySQL read operation
          try {
               $connection = new PDO(DB_DSN, DB_USER, DB_PASS, DB_OPTION);    

               $statement = $connection->prepare($sql);

               foreach($filter as $key => $val)
               {               
                    $statement->bindParam(':'.$key, $val, PDO::PARAM_STR);
               }
               $statement->execute();

               $result = $statement->fetch();

               return $result;

          } catch(PDOException $error) {
               return $sql . "<br>" . $error->getMessage();
          }
     }

}
?>